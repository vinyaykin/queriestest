-- Inserting students for testing purposes
INSERT INTO student (id, name, age)
VALUES ('3f877742-33cb-460e-8041-e9ce5fb04bdb', 'Maxim', 18);

INSERT INTO student (id, name, age)
VALUES ('15aa9bbc-c00e-4bb4-a037-0aa5f68d6bad', 'John', 8);

INSERT INTO student (id, name, age)
VALUES ('bcf37635-2664-4cb6-b238-847ebdf74837', 'Pole', 20);

INSERT INTO student (id, name, age)
VALUES ('f388685f-f179-4012-bde9-a1c7a2e6d9cf', 'Olga', 17);

INSERT INTO student (id, name, age)
VALUES ('07b61044-d1ac-494d-986c-f91a1d94d23f', 'Polina', 14);

-- Inserting courses for testing purposes
INSERT INTO course (id, name, description)
VALUES ('f8250872-7c9b-4ce3-9a76-79724b67b4c8', 'Course for boys', 'Only boys are allowed');

INSERT INTO course (id, name, description)
VALUES ('5f8ff950-8db9-4d00-8f02-470f8cbc7d2e', 'Course for girls and Maxim', 'Only girls and Maxim are allowed');

-- Mapping students to courses
INSERT INTO student_course (student_id, course_id)
VALUES ('3f877742-33cb-460e-8041-e9ce5fb04bdb', 'f8250872-7c9b-4ce3-9a76-79724b67b4c8');

INSERT INTO student_course (student_id, course_id)
VALUES ('15aa9bbc-c00e-4bb4-a037-0aa5f68d6bad', 'f8250872-7c9b-4ce3-9a76-79724b67b4c8');

INSERT INTO student_course (student_id, course_id)
VALUES ('bcf37635-2664-4cb6-b238-847ebdf74837', 'f8250872-7c9b-4ce3-9a76-79724b67b4c8');

INSERT INTO student_course (student_id, course_id)
VALUES ('f388685f-f179-4012-bde9-a1c7a2e6d9cf', '5f8ff950-8db9-4d00-8f02-470f8cbc7d2e');

INSERT INTO student_course (student_id, course_id)
VALUES ('07b61044-d1ac-494d-986c-f91a1d94d23f', '5f8ff950-8db9-4d00-8f02-470f8cbc7d2e');

INSERT INTO student_course (student_id, course_id)
VALUES ('3f877742-33cb-460e-8041-e9ce5fb04bdb', '5f8ff950-8db9-4d00-8f02-470f8cbc7d2e')