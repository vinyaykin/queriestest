CREATE TABLE student
(
    id     UUID PRIMARY KEY,
    name   VARCHAR(64) NOT NULL,
    age    INT         NOT NULL
);

CREATE TABLE course
(
    id          UUID PRIMARY KEY,
    name        TEXT NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE student_course
(
    student_id UUID NOT NULL,
    course_id  UUID NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (id),
    FOREIGN KEY (course_id) REFERENCES course (id),
    UNIQUE (student_id, course_id)
)
