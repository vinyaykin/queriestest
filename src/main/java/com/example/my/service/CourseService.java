package com.example.my.service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.example.my.model.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;
import com.example.my.dao.CourseRepository;
import com.example.my.model.Course;

@Service
public class CourseService {

    private final CourseRepository repository;

    public CourseService(CourseRepository repository) {
        this.repository = repository;
    }

    public void save(Course course) {
        repository.save(course);
    }

    public List<Course> findAll() {
        return repository.findAll();
    }

    public void addStudent(Course course, Student student) { repository.addStudent(course, student); }


    public void deleteAllStudentCourse() {
        repository.deleteAllStudentCourse();
    }

    public void deleteAllCourse() {
        repository.deleteAllCourse();
    }

    public void dropTemp() {
        repository.dropTemp();
    }

    public Course findCourseWithMaxAvgAgeStudent1() {
        return repository.findCourseWithMaxAvgAge1().orElseThrow();
    }

    public Course findCourseWithMaxAvgAgeStudent2() {
        repository.temp();
        return repository.findCourseWithMaxAvgAge2();
    }

    public Course findCourseWithMaxAvgAgeStudent3() {
        return repository.findCourseWithMaxAvgAge3();
    }
}
