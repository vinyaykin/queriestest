package com.example.my.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.example.my.dao.StudentRepository;
import com.example.my.model.Student;

@Service
public class StudentService {

    private final StudentRepository repository;

    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public void create(Student student) {
        repository.create(student);
    }

    public void save(Student student) {
        repository.save(student);
    }

    public List<Student> findAll() {
        return repository.findAll();
    }

    public void deleteAllStudent() {
        repository.deleteAllStudent();
    }
}
