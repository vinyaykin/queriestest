package com.example.my.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import com.example.my.model.Student;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface StudentRepository {

    @Insert("INSERT INTO student (id, name, age) VALUES (#{id}, #{name}, #{age})")
    void create(Student student);

    @Insert("INSERT INTO student (id, name, age) VALUES (#{id}, #{name}, #{age})")
    void save(Student student);

    @Select("SELECT * FROM student")
    @Results(value = {
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "age", column = "age")
    })
    List<Student> findAll();

    @Delete("DELETE FROM student")
    void deleteAllStudent();
}
