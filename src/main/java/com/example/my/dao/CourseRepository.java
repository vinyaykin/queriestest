package com.example.my.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.my.model.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import com.example.my.model.Course;

@Mapper
public interface CourseRepository {


    @Insert("INSERT INTO course (id, name, description) VALUES (#{id}, #{name}, #{description})")
    void save(Course course);


    @Select("SELECT * FROM course")
    @Results(value = {
        @Result(property = "course", column = "course")
    })
    List<Course> findAll();


    @Insert("INSERT INTO student_course (student_id, course_id) VALUES (#{student.id}, #{course.id})")
    void addStudent(Course course, Student student);

    @Delete("DELETE FROM student_course")
    void deleteAllStudentCourse();

    @Delete("DELETE FROM course")
    void deleteAllCourse();

    @Delete("DROP TABLE IF EXISTS temp")
    void dropTemp();


    @Select("""
            SELECT course.id,
                   course.name,
                   course.description
            FROM course,
                 student,
                 student_course
            WHERE course.id = student_course.course_id
              AND student.id = student_course.student_id
            GROUP BY course.id
            ORDER BY AVG(student.age)
                    DESC
            LIMIT 1
            """)
    @Results(value = {
        @Result(column = "id", property = "id"),
        @Result(column = "name", property = "name"),
        @Result(column = "description", property = "description")
    })
    Optional<Course> findCourseWithMaxAvgAge1();


    @Insert("""
            CREATE TEMPORARY TABLE IF NOT EXISTS temp AS
            (SELECT c.name, sc.student_id FROM course c
            LEFT JOIN student_course sc
            ON sc.course_id = c.id)
            """)
    void temp();

    @Select("""
            SELECT TOP(1) course_name.name, AVG(s.age) as MaxAvgAge
                    FROM student s
                        LEFT JOIN temp course_name
                        ON course_name.student_id = s.id
                    GROUP BY
                        course_name.name
                    ORDER BY
                        AVG(s.age) DESC
            """)
    @Results(value = {
        @Result(property = "name", column = "name"),
    })
    Course findCourseWithMaxAvgAge2();

    @Select("""
            SELECT course.*
                              FROM course
                                       JOIN student_course ON student_course.course_id = course.id
                                       JOIN student ON student.id = student_course.student_id
                              GROUP BY course.name, course.id
            having AVG(student.age) =\s
            (SELECT AVG(student.age) avg_age
                              FROM course
                                       JOIN student_course ON student_course.course_id = course.id
                                       JOIN student ON student.id = student_course.student_id
                              GROUP BY course.name, course.id
            ORDER BY avg_age DESC LIMIT 1)
            LIMIT 1
            """)
    Course findCourseWithMaxAvgAge3();
}
