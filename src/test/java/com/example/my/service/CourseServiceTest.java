package com.example.my.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import com.example.my.model.Course;
import com.example.my.model.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CourseServiceTest {

    @Autowired
    StudentService studentService;
    @Autowired
    CourseService courseService;

    private Course courseWithMaxAvgExpected;

    @BeforeAll
    void init() throws IOException {
        courseService.deleteAllStudentCourse();
        courseService.deleteAllCourse();
        courseService.dropTemp();
        studentService.deleteAllStudent();
        Scanner scannerNames = new Scanner(new File("src/main/resources/names.txt"));
        List<Student> students = new ArrayList<>();
        Random rand = new Random();
        while (scannerNames.hasNext()) {
            students.add(
                new Student(UUID.randomUUID(), scannerNames.next(), rand.nextInt(95))
            );
        }
        scannerNames.close();

        Scanner scannerCourses = new Scanner(new File("src/main/resources/courses.csv"));
        List<Course> courses = new ArrayList<>();
        while (scannerCourses.hasNext()) {
            String[] courseName = scannerCourses.next().split(";");
            if (courseName.length < 2) {
                continue;
            }
            courses.add(new Course(UUID.randomUUID(), courseName[0], courseName[1]));
        }
        scannerCourses.close();

        students.forEach(student -> studentService.save(student));
        courses.forEach(course -> courseService.save(course));
        Map<Course, List<Student>> courseMap = new HashMap<>();
        students.forEach(student -> {
            var course = courses.get(rand.nextInt(courses.size()));
            courseService.addStudent(course, student);
            courseMap.putIfAbsent(course, new ArrayList<>());
            courseMap.get(course).add(student);
        });
        courseWithMaxAvgExpected = courseMap.entrySet().stream()
            .map(entry -> Map.entry(
                entry.getKey(),
                entry.getValue().stream().mapToInt(Student::getAge).sum() / entry.getValue().size()
            ))
            .reduce((e1, e2) -> e1.getValue() > e2.getValue() ? e1 : e2)
            .orElseThrow()
            .getKey();
        log.info("Number of students {}", students.size());
        log.info("Number of courses {}", courses.size());
    }

    @Test
    void findCourseWithMaxAvgAge_Сontest() throws IOException {
        Course actual1 = timer(() -> courseService.findCourseWithMaxAvgAgeStudent1(), "Method 1, time {}ms.");
        Course actual2 = timer(() -> courseService.findCourseWithMaxAvgAgeStudent2(), "Method 2, time {}ms.");
        Course actual3 = timer(() -> courseService.findCourseWithMaxAvgAgeStudent3(), "Method 3, time {}ms.");


        assertEquals(courseWithMaxAvgExpected, actual1);
        assertEquals(courseWithMaxAvgExpected.getName(), actual2.getName());
        assertEquals(courseWithMaxAvgExpected.getName(), actual2.getName());
    }

    private static <T> T timer(Supplier<T> method, String name) {
        long m = System.currentTimeMillis();
        T result = method.get();
        log.info(name, System.currentTimeMillis() - m);

        return result;
    }
}
